import {VisitCardiologist, VisitDentist, VisitTherapist} from "./Visit";

const visits = [
  new VisitDentist({
    title: "visit to dentist",
    status: "done",
    doctor: "dentist",
    purpose: "visit to dentist",
    desc: "dfjbnf",
    priority: "high",
    fullName: "ololo",
    lastDateVisit: "12.07.2007"
  }),
  new VisitDentist({
    title: "visit to dentist",
    status: "open",
    doctor: "dentist",
    purpose: "visit to dentist",
    desc: "dfjbnf",
    priority: "normal",
    fullName: "ololo",
    lastDateVisit: "12.07.2007"
  }),
  new VisitDentist({
    title: "visit to dentist",
    status: "done",
    doctor: "dentist",
    purpose: "visit to dentist",
    desc: "dfjbnf",
    priority: "low",
    fullName: "ololo",
    lastDateVisit: "12.07.2007"
  }),
  new VisitCardiologist({
    title: "visit to cardiologist",
    status: "done",
    doctor: "cardiologist",
    purpose: "visit to dentist",
    desc: "dfjbnf",
    priority: "high",
    fullName: "ololo",
    lastDateVisit: "12.07.2007"
  }),
  new VisitCardiologist({
    title: "visit to cardiologist",
    status: "open",
    doctor: "cardiologist",
    purpose: "visit to dentist",
    desc: "dfjbnf",
    priority: "normal",
    fullName: "ololo",
    lastDateVisit: "12.07.2007"
  }),
  new VisitCardiologist({
    title: "visit to cardiologist",
    status: "open",
    doctor: "cardiologist",
    purpose: "visit to dentist",
    desc: "dfjbnf",
    priority: "low",
    fullName: "ololo",
    lastDateVisit: "12.07.2007"
  }),
  new VisitCardiologist({
    title: "visit to cardiologist",
    status: "done",
    doctor: "cardiologist",
    purpose: "visit to dentist",
    desc: "dfjbnf",
    priority: "high",
    fullName: "ololo",
    lastDateVisit: "12.07.2007"
  }),
  new VisitTherapist({
    title: "visit to therapist",
    status: "done",
    doctor: "therapist",
    purpose: "visit to dentist",
    desc: "dfjbnf",
    priority: "high",
    fullName: "ololo",
    lastDateVisit: "12.07.2007"
  }),
  new VisitTherapist({
    title: "visit to therapist",
    status: "open",
    doctor: "therapist",
    purpose: "visit to dentist",
    desc: "dfjbnf",
    priority: "normal",
    fullName: "ololo",
    lastDateVisit: "12.07.2007"
  }),
  new VisitTherapist({
    title: "visit to therapist",
    status: "open",
    doctor: "therapist",
    purpose: "visit to dentist",
    desc: "dfjbnf",
    priority: "low",
    fullName: "ololo",
    lastDateVisit: "12.07.2007"
  }),
  new VisitTherapist({
    title: "visit to therapist",
    status: "done",
    doctor: "therapist",
    purpose: "visit to dentist",
    desc: "dfjbnf",
    priority: "high",
    fullName: "ololo",
    lastDateVisit: "12.07.2007"
  })
];

/// - отрендерить массивчик на страницу
// visits.forEach(v => v.render(container));


//////////// - записать массивчик на сервер
// visits.forEach(v=>createCard(v));